
import pprint
import random
import numpy as np

# Recursive edges are not allowed currently.
class Network:
    
    def __init__(self, numNodes):
        self.NumNodes = numNodes
        self.Clear()
    
    def AddEdge(self, src, dst, edgeWeight = 1, force=False):
        if src != dst:
            if self.Edges[src,dst] == 0:
                self.Edges[src,dst] = edgeWeight
                self.Edges[src,0] += 1
                self.Edges[0,dst] += 1
                self.NumEdges += 1
                return True
            elif force:
                self.Edges[src,dst] = edgeWeight
                return True
        return False
    
    def Clear(self):
        self.NumEdges = 0
        self.Edges = np.zeros((self.NumNodes + 1, self.NumNodes + 1))
    
    def InDegree(self,nodeIndex):
        return self.Edges[nodeIndex,0]
    
    def OutDegree(self,nodeIndex):
        return self.Edges[0,nodeIndex]
    
    def __str__(self):
        return ("--Network of " + str(self.NumNodes) + \
            " Nodes--\nHas "+ str(self.NumEdges) +" Edges:\n" \
                + pprint.pformat(self.Edges) + "\n---***---")
    
    def __repr__(self):
        return str(self)

class ScaleFreeNetwork:
    def __init__(self,maxNodes):
        self.network = Network(maxNodes)
        self.NumCurrentNodes = 0
    
    def Clear(self):
        self.network.Clear()
        self.NumCurrentNodes = 0
    
    def PickNodeByOutDegree(self):
        # 1 <= rnd <= (E + N)
        rnd = random.randint(1,self.network.NumEdges + self.NumCurrentNodes)
        i=0
        while rnd > 0:
            i += 1
            #print(str(i) + " -> " + str(rnd))
            rnd = rnd - self.network.Edges[i,0] - 1
        return i
    
    def PickNodeByInDegree(self):
        rnd = random.randint(1,self.network.NumEdges + self.NumCurrentNodes)
        i=0
        while rnd > 0:
            i += 1
            #print(str(i) + " -> " + str(rnd))
            rnd = rnd - self.network.Edges[0,i] - 1
        return i
    
    def AddEdgeToNewNodePA(self):
        src = self.PickNodeByOutDegree()
        self.NumCurrentNodes += 1
        return self.network.AddEdge(src,self.NumCurrentNodes)
    
    def AddEdgeFromNewNodePA(self):
        dst = self.PickNodeByInDegree()
        self.NumCurrentNodes += 1
        return self.network.AddEdge(self.NumCurrentNodes,dst)
    
    def AddEdgePA(self):
        if self.NumCurrentNodes < 2:
            self.NumCurrentNodes = 2
            return self.network.AddEdge(1,2)
        else:
            src = self.PickNodeByOutDegree()
            dst = self.PickNodeByInDegree()
            return self.network.AddEdge(src,dst)
    
    def __str__(self):
        return ("--Scale-Free Network of " + str(self.NumCurrentNodes) + \
            " Nodes (Max Nodes: " + str(self.network.NumNodes) +")"
            " --\nHas "+ str(self.network.NumEdges) +" Edges:\n" \
                + pprint.pformat(self.network.Edges) + "\n---***---")
    
    def __repr__(self):
        return str(self)


class ScaleFreeNetworkGenerator :
    def __init__(self, sfn, alpha = 1.0/3.0, beta = 1.0/3.0, \
            runCondition = lambda x: True):
        self.sfnetwork = sfn
        self.Check_alpha = alpha
        self.Check_beta = alpha + beta
        self.RunCondition = runCondition
        self.NumIterations = 0
    
    def CreateALink(self):
        rn = random.random()
        if rn < self.Check_alpha :
            self.sfnetwork.AddEdgeFromNewNodePA()
        elif rn < self.Check_beta:
            ### *** This may not be possible when edges are saturated!!!
            done = self.sfnetwork.AddEdgePA()
        else: #gamma
            self.sfnetwork.AddEdgeToNewNodePA()
    
    def Generate(self):
        if self.sfnetwork.NumCurrentNodes < 1 :
            self.NumIterations += 1
            self.sfnetwork.AddEdgePA()
        while self.sfnetwork.NumCurrentNodes < self.sfnetwork.network.NumNodes and self.RunCondition(self):
            self.NumIterations += 1
            self.CreateALink()
    
    def __str__(self):
        return "--Scale-Free Network Generator--\n" + \
            "\nAlpha : " + str(self.Check_alpha) + \
            "\nBeta : " + str(self.Check_beta - self.Check_alpha) + \
            "\nGamma : " + str(1 - self.Check_beta) + \
            "\nCurrent Number of Iterations : " + str(self.NumIterations)
    
    def __repr__(self):
        return str(self)
    

sfn = ScaleFreeNetwork(5000)
g = ScaleFreeNetworkGenerator(sfn,alpha=0.025,beta=0.95)
#print(sfn)
#print(g)
g.Generate()
#print(sfn)
