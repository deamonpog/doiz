import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
ltmout = pd.read_csv('Output 1000 ICM_ABM_with SFN generation 2 experiment_LTM-table.csv',skiprows=6)
ltmout.drop(columns=['[run number]','people-count','[step]','stop-ticks','count links','alpha','beta'],inplace=True)
ltmout.drop(columns=['count people'],inplace=True)
ltmout = ltmout[ltmout['ltm-weight'] != 0]
ltmout = ltmout[ltmout['ltm-threshold'] != 0]
ltmout = ltmout[ltmout['ltm-threshold'] != 1]
ltmout = ltmout[ltmout['initial-infects'] != 0]
ltmout['ratio'] = ltmout['ltm-threshold'] / ltmout['ltm-weight']


plt.scatter(ltmout['ratio'],ltmout['count people with [ m_state = "I"] / count people'],alpha=0.3)
plt.title('LTM')
plt.xlabel('threshold / weight ratio')
plt.ylabel('fraction of infection')
plt.ylim(0,1)
plt.show()



fig, ax = plt.subplots()

temp = ltmout[(ltmout['initial-infects'] < 0.1)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:blue',label='initial-infects < 0.1',alpha=0.3)

temp = ltmout[(ltmout['initial-infects'] >= 0.1) & (ltmout['initial-infects'] < 0.2)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:green',label='0.1 <= initial-infects < 0.2',alpha=0.3)

temp = ltmout[(ltmout['initial-infects'] >= 0.2) & (ltmout['initial-infects'] < 0.9)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:orange',label='0.2 <= initial-infects < 0.9',alpha=0.3)


temp = ltmout[(ltmout['initial-infects'] >= 0.9)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:red',label='0.9 <= initial-infects',alpha=0.3)

ax.legend()

plt.title('Linear Threshold Model Dynamics')
plt.xlabel('Threshold / Weight Ratio')
plt.ylabel('Fraction of Infection')
plt.show()




fig, ax = plt.subplots()

temp = ltmout[(ltmout['ratio'] <= 1)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:blue',label='1 edge is enough',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 1) & (ltmout['ratio'] <= 2)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:green',label='at least 2 edges needed',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 2) & (ltmout['ratio'] <= 3)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:orange',label='at least 3 edges needed',alpha=0.3)


temp = ltmout[(ltmout['ratio'] > 3)]
ax.scatter(temp['ratio'],temp['count people with [ m_state = "I"] / count people'], c='tab:red',label='more than 3 edges needed',alpha=0.3)

ax.legend()

plt.title('Linear Threshold Model Dynamics')
plt.xlabel('Threshold / Weight Ratio')
plt.ylabel('Fraction of Infection')
plt.show()



fig, ax = plt.subplots()

temp = ltmout[(ltmout['ratio'] <= 1)]
ax.scatter(temp['initial-infects'],temp['count people with [ m_state = "I"] / count people'], c='tab:blue',label='1 edge is enough',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 1) & (ltmout['ratio'] <= 2)]
ax.scatter(temp['initial-infects'],temp['count people with [ m_state = "I"] / count people'], c='tab:green',label='at least 2 edges needed',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 2) & (ltmout['ratio'] <= 3)]
ax.scatter(temp['initial-infects'],temp['count people with [ m_state = "I"] / count people'], c='tab:orange',label='at least 3 edges needed',alpha=0.3)


temp = ltmout[(ltmout['ratio'] > 3)]
ax.scatter(temp['initial-infects'],temp['count people with [ m_state = "I"] / count people'], c='tab:red',label='more than 3 edges needed',alpha=0.3)

ax.legend()

plt.title('Linear Threshold Model Dynamics')
plt.xlabel('initial-infects')
plt.ylabel('Fraction of Infection')
plt.show()






fig, ax = plt.subplots()

temp = ltmout[(ltmout['ratio'] <= 1)]
ax.scatter(temp['count people with [ m_state = "I"] / count people'],temp['ratio'], c='tab:blue',label='1 edge is enough',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 1) & (ltmout['ratio'] <= 2)]
ax.scatter(temp['count people with [ m_state = "I"] / count people'],temp['ratio'], c='tab:green',label='at least 2 edges needed',alpha=0.3)

temp = ltmout[(ltmout['ratio'] > 2) & (ltmout['ratio'] <= 3)]
ax.scatter(temp['count people with [ m_state = "I"] / count people'], temp['ratio'],c='tab:orange',label='at least 3 edges needed',alpha=0.3)


temp = ltmout[(ltmout['ratio'] > 3)]
ax.scatter(temp['count people with [ m_state = "I"] / count people'], temp['ratio'],c='tab:red',label='more than 3 edges needed',alpha=0.3)

ax.legend()

plt.title('Linear Threshold Model Dynamics')
plt.ylabel('Threshold / Weight Ratio')
plt.xlabel('Fraction of Infection')
plt.show()
